import requests
import json
import base64

NODEFLUX_ACCESS_KEY = "XJNH819PC42VP96EG5PE2QVBE"
NODEFLUX_SECRET_KEY = "s6T5qfdt5LmlRM6GH6zI7-FKzpBnUpfvzuOE_9QcvwGQdIQhECNqP0qMMs3IoIkG"

signatureUrl = "https://backend.cloud.nodeflux.io/auth/signatures"
# Face Demography URL
FaceDemoUrl = "https://api.cloud.nodeflux.io/v1beta1/image-analytic/stream"

token = "af1b30e2c69a9308ed74a9bd3782e4d045350771afded85f5017e7ff74fdf606"

def getAccessToken():
    jsonKey = {
        "access_key": NODEFLUX_ACCESS_KEY,
        "secret_key": NODEFLUX_SECRET_KEY
    }
    r = requests.post(signatureUrl, json=jsonKey)
    print("========== Get Access Token ==========")
    print(r)
    print("\nStatus code = " , r.status_code)
    print("\nresult = " ,  r.text)

getAccessToken()


def fdTesting():

    with open("face_demo.jpg", "rb") as f:
        face_demo_content = base64.b64encode(f.read()).decode('utf-8')

    headers = {
        "Content-type":
        "json",
        "Authorization":
        "NODEFLUX-HMAC-SHA256 Credential=" + NODEFLUX_ACCESS_KEY +
        "/20200417/nodeflux.api.v1beta1.ImageAnalytic/StreamImageAnalytic, SignedHeaders=x-nodeflux-timestamp, Signature="
        + token,
        "x-nodeflux-timestamp":
        "20200417T112215Z"
    }

    data = {
        "analytics": [{
            "type": "FACE_DEMOGRAPHY"
        }
        ],
        "image": {
            "encoding": "IMAGE_ENCODING_JPEG",
            "content": face_demo_content
        }
    }
    r = requests.post(FaceDemoUrl, headers=headers, data=json.dumps(data))
    print("\n========== Face Demography Analytic ==========")
    print(r)
    print("\nStatus code = " , r.status_code)
    print("\n",r.text)

fdTesting()
